#!/bin/sh

echo "Create RC Branch from Staging"
RC_VERSION="$(buildkite-agent meta-data get "rc-version")"
LEADING_VERSION="$(buildkite-agent meta-data get "leading-version")"
##RC_VERSION=$1 && echo "RC_VERSION=${RC_VERSION}"
##LEADING_VERSION=$2 && echo "LEADING_VERSION=${LEADING_VERSION}"

RC_BRANCH_NAME="release/RC-${RC_VERSION}"
MANIFEST_PATH="com.mfouruau.releasepls/src"

## Create RC Branch from Staging
git status
git branch ${RC_BRANCH_NAME}
git checkout ${RC_BRANCH_NAME}

## Update Version
cd ${MANIFEST_PATH}
echo ${PWD}
sed -i temp -e "s/${LEADING_VERSION}/${RC_VERSION}/g" manifest.xml

## Commit and push
git add manifest.xml
git commit -m "[R] Update project version from ${LEADING_VERSION} to ${RELEASE_VERSION}"
git push --set-upstream origin ${RC_BRANCH_NAME}
