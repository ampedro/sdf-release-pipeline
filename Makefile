.PHONY: test

build-docker:
	@echo "+++ Run npm test"
	git rev-parse HEAD > BUILD_ID
	docker-compose build

build-validate:
	@echo "+++ Validate and Run Tests"
	docker-compose build app-validate

build-rc:
	@echo "+++ Create RC Branch"
	deploy/build-rc.sh
	docker-compose build app-validate

build-prod:
	@echo "+++ Release"
	deploy/build-prod.sh
	docker-compose build app-validate

deploy-qa:
	@echo "+++ Deploy to QA"
	deploy/deploy-qa.sh
	docker-compose build app-validate

clean-docker:
	docker-compose rm -fsv
